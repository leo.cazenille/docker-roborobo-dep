FROM lcazenille/ubuntupysshgcc:latest
MAINTAINER leo.cazenille@gmail.com

ENV DEBIAN_FRONTEND noninteractive

RUN \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -yq wget unzip libeigen3-dev libboost-all-dev automake mercurial cmake libsdl2-2.0 libsdl2-dev libsdl2-image-dev && \
    apt-get purge -yq libsdl2-2.0 libsdl2-dev libsdl2-image-dev && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install --upgrade --no-cache-dir git+https://gitlab.com/leo.cazenille/qdpy.git@master deap>=1.2.2 tqdm>=4.28.1 colorama>=0.4.1 cma>=2.6.0

RUN \
    hg clone https://hg.libsdl.org/SDL && cd SDL && mkdir build && cd build && cmake -DVIDEO_OFFSCREEN=ON -DCMAKE_INSTALL_PREFIX=/usr .. && make -j 20 && make install && cd ../..

RUN \
    hg clone http://hg.libsdl.org/SDL_image && cd SDL_image && autoreconf -f -i; ./configure --prefix=/usr && make -j 20 && make install && cd ..


CMD ["bash"]

# MODELINE	"{{{1
# vim:expandtab:softtabstop=4:shiftwidth=4:fileencoding=utf-8
# vim:foldmethod=marker
